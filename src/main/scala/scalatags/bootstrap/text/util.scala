package scalatags.bootstrap.text

import scalatags.Text.all._
import scalatags.generic.AttrPair
import scalatags.text.Builder

object util {
  def aria(key: String, value: String): AttrPair[Builder, String] = s"aria-$key".attr := value
  
  def data(key: String, value: String): AttrPair[Builder, String] = s"data-$key".attr := value
  
  val method  = "method".attr
  val role    = "role".attr
}

/**
 * Common attributes of META tags.
 */
object meta {
  val content   = "content".attr
  val httpEquiv = "http-equiv".attr
  val name      = "name".attr
}
