package scalatags.bootstrap.text

import scalatags.bootstrap.text.util._
import scalatags.bootstrap.text.util.role
import scalatags.fontawesome.text.icons._

import scalatags.Text.all._
import scalatags.Text.{tags => t}
import scalatags.text.Frag

/**
 * Tags to support Twitter Bootstrap
 * Synced with version 3.1.1
 */
object all {
  //----------------------------------------------------------------------------
  // Navigation
  //----------------------------------------------------------------------------
  
  val breadCrumb = ol(cls := "breadcrumb")

  def breadCrumbItem(
    itemHref: String,
    caption: String,
    active: Boolean
  ) =
    if (active) {
      li(cls := "active")(caption)
    } else {
      li(
        a(href := itemHref)(caption)
      )
    }

  def navbar(content: Frag*) =
    div(cls := "navbar navbar-default navbar-fixed-top",
      "role".attr := "navigation")(
        div(cls := "container")(content)
      )

  val navbarCollaps = div(cls := "collapse navbar-collapse")

  val navbarNav = div(cls := "nav navbar-nav")
  
  val navbarNavRight = div(cls := "nav navbar-nav navbar-right")
  
  def navbarItem(
    itemHref: String,
    caption: String,
    active: Boolean
  ) =
    li(
      cls := (if(active) "active" else "")
    )(
      a(href := itemHref)(caption)
    )
  
  
  val navbarHeader = div(cls := "navbar-header")

  def navbarToggle(caption: String) =
    t.button(cls := "navbar-toggle",
      tpe := "button",
      data("toggle", "collapse"),
      data("target", ".navbar-collapse")
    )(
      // TODO: Can this be included?
      //span(cls := "sr-only")(caption),
      icon(IconType.BARS)
    )

  def navbarBrand(brandName: String, brandHref: String = "/") =
    a(cls := "navbar-brand", href := brandHref)(brandName)

  def pageHeader(title: String) =
    div(cls := "page-header")(
      h1(title)
    )
  
  def pageHeader(content: Frag*) =
    div(cls := "page-header")(
      h1(content)
    )
  
  //----------------------------------------------------------------------------
  // Components
  //----------------------------------------------------------------------------

  def panel(title: String)(content: Frag*) =
    div(cls := "panel panel-default")(
      div(cls := "panel-heading")(
        h3(cls := "panel-title")(title)
      ),
      div(cls := "panel-body")(
        content
      )
    )
  

  //----------------------------------------------------------------------------
  // Alerts
  //----------------------------------------------------------------------------
  case class AlertType(value: String) {
    override def toString = s"alert $value"
  }

  object AlertType {
    val SUCCESS = AlertType("alert-success")
    val INFO    = AlertType("alert-info")
    val WARNING = AlertType("alert-warning")
    val DANGER  = AlertType("alert-danger")
  }
  
  object alert {
    def apply(
      alertType: AlertType = AlertType.DANGER
    )(content: Frag*): Frag =
      makeAlert(
        alertType     = alertType,
        isDismissable = false
      )(content)
    
    def dismissable(
      alertType: AlertType = AlertType.DANGER
    )(content: Frag*): Frag =
      makeAlert(
        alertType     = alertType,
        isDismissable = true
      )(content)
    
    private def makeAlert(
      alertType:      AlertType,
      isDismissable:  Boolean
    )(content: Seq[Frag]): Frag = 
      div(cls:=alertType.toString, role:="alert")(
        t.button(
          tpe:="button",
          cls:="close",
          data(key="dismiss",  value="alert")
        )(
          span(aria(key="hidden", value="true"))(raw("&times;")),
          span(cls:="sr-only")("Close")
        ),
        
        content
      )
  }
  
  //----------------------------------------------------------------------------
  // Buttons
  //----------------------------------------------------------------------------
  
  object button {
    def apply(
      buttonType: ButtonType = ButtonType.DEFAULT,
      buttonSize: ButtonSize = ButtonSize.DEFAULT,
      isBlock:    Boolean    = false
    )(value: Frag*) = button.makeButton("button", buttonType, buttonSize, isBlock)(value)

    def submit(
      buttonType: ButtonType = ButtonType.DEFAULT,
      buttonSize: ButtonSize = ButtonSize.DEFAULT,
      isBlock:    Boolean    = false
    )(value: Frag*) = button.makeButton("submit", buttonType, buttonSize, isBlock)(value)

    def link(
      target: String = "#",
      buttonType: ButtonType = ButtonType.DEFAULT,
      buttonSize: ButtonSize = ButtonSize.DEFAULT
    )(value: Frag*): Frag =
      makeLink(
        target,
        buttonType,
        buttonSize
      )(value)
    
    private def makeButton(
      htmlButtonType: String,
      buttonType: ButtonType,
      buttonSize: ButtonSize,
      isBlock:    Boolean
    )(value: Seq[Frag]): Frag = 
      t.button(
        tpe := htmlButtonType,
        cls := Seq(buttonType, buttonSize, if (isBlock) "btn-block").mkString(" ")
      )(value)

    private def makeLink(
      target: String,
      buttonType: ButtonType,
      buttonSize: ButtonSize
    )(value: Seq[Frag]): Frag =
      a(
        href := target,
        cls := Seq(buttonType, buttonSize).mkString(" ")
      )(value)
  }

  case class ButtonType(value: String) {
    override def toString = s"btn $value"
  }

  object ButtonType {
    val DEFAULT = ButtonType("btn-default")
    val PRIMARY = ButtonType("btn-primary")
    val SUCCESS = ButtonType("btn-success")
    val INFO    = ButtonType("btn-info")
    val WARNING = ButtonType("btn-warning")
    val DANGER  = ButtonType("btn-danger")
    val LINK    = ButtonType("btn-link")
  }

  case class ButtonSize(value: String) {
    override def toString = value
  }

  object ButtonSize {
    val DEFAULT = ButtonSize("")
    val LARGE   = ButtonSize("btn-lg")
    val SMALL   = ButtonSize("btn-sm")
    val XSMALL  = ButtonSize("btn-xs")
  }

}
