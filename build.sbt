organization := "com.scalatags"

name := "scalatags-bootstrap"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.2"

libraryDependencies ++= Seq(
  "com.scalatags" %% "scalatags"             % "0.4.2",
  "com.scalatags" %% "scalatags-fontawesome" % "0.1.0-SNAPSHOT"
)

val mavenBucket = "s3://maven.wicondev.se.s3.amazonaws.com/"

resolvers += "WiconDev Snapshots" at mavenBucket + "snapshots"

resolvers += "WiconDev Releases"  at mavenBucket + "releases"

publishTo <<= version { (v: String) =>
  if (v.trim.endsWith("SNAPSHOT")) 
    Some("snapshots" at mavenBucket + "snapshots") 
  else
    Some("releases"  at mavenBucket + "releases")
}

publishMavenStyle := true

publishArtifact in Test := false

pomIncludeRepository := { _ => false }
